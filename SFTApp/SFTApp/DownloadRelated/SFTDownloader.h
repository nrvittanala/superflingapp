//
//  SFTDownloader.h
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SFTDownloaderDelegate <NSObject>


-(void) OnDataDownloadFailed:(NSError*)error;
-(void) OnDataDownload:(NSDictionary*) data;
-(void) OnDownloadImage:(UIImage*) image ImageID:(NSString*)sID Bytes:(NSUInteger) Bytes OrgWidth:(CGFloat)nWidth ;


@end

@interface SFTDownloader : NSObject

@property (nonatomic,strong) id<SFTDownloaderDelegate> delegate;
@property (nonatomic, strong) NSOperationQueue *operationQueue;

-(void) downloadData:(NSString*) sURL;
-(void) downloadImage:(NSString*) sID;



@end
