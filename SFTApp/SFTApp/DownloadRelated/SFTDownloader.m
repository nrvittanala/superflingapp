//
//  SFTDownloader.m
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import "SFTDownloader.h"
#import "SFTDefs.h"


@implementation SFTDownloader

-(id) init{
    self=[super init];
    [self intializeAll];
    return self;
}

-(void) intializeAll
{
    self.operationQueue=[[NSOperationQueue alloc] init];
    
}


-(void) downloadData:(NSString*) sURL
{
    NSBlockOperation *datadownloadOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        
        NSURL *url=[NSURL URLWithString:sURL];
        
        NSURLRequest  *request=[NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:self.operationQueue
                               completionHandler:^(NSURLResponse * response,
                                                   NSData * data,
                                                   NSError * error) {
                                   if (error == nil)
                                   {
                                       NSError* error1;
                                       NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                            options:kNilOptions
                                                                                              error:&error1];
                                       [self.delegate OnDataDownload:json];
                                       
                                   }
                                   else
                                   {
                                       [self.delegate OnDataDownloadFailed:error];
                                   }
                               }];
        
        
    }];
    
    
    [self.operationQueue addOperation:datadownloadOperation];
    
    
}

-(void) downloadImage:(NSString*) strImageID
{
      NSBlockOperation *imagedownloadOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        
        NSString *sURL=[NSString stringWithFormat:kImgDownloadURL,strImageID];
        
        NSURL *url=[NSURL URLWithString:sURL];
        
        NSURLRequest  *request=[NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:self.operationQueue
                               completionHandler:^(NSURLResponse * response,
                                                   NSData * data,
                                                   NSError * error) {
                                   if (error == nil)
                                   {
                                       UIImage *image = [[UIImage alloc] initWithData:data];
                                       
                                       UIImage *Imagenew = [self resizeImage:image];
                                       
                                       NSData *data2 = UIImageJPEGRepresentation(image, 1.0);
                                       
                                       [self.delegate OnDownloadImage:Imagenew ImageID:strImageID Bytes:[data2 length]  OrgWidth:image.size.width];
                                       
                                   }
                                   else
                                   {
                                       [self.delegate OnDataDownloadFailed:error];
                                   }
                                   
                               }];
        
        
    }];
    

    [self.operationQueue addOperation:imagedownloadOperation];
    
  }


- (UIImage*)resizeImage:(UIImage*)image
{
    CGFloat height=(image.size.height/image.size.width)*GET_WIDTH;
    CGFloat width=(GET_WIDTH);
    // Create a graphics context with the target size
    // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
    // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
    CGSize size = CGSizeMake(width, height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Flip the context because UIKit coordinate system is upside down to Quartz coordinate system
    CGContextTranslateCTM(context, 0.0, height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // Draw the original image to the context
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, width, height), image.CGImage);
    
    // Retrieve the UIImage from the current context
    UIImage *imageOut = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return imageOut;
}



@end
