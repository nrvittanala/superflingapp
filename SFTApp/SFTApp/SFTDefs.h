//
//  SFTDefs.h
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#ifndef SFTApp_SFTDefs_h
#define SFTApp_SFTDefs_h


#define IS_IPAD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPad" ] ||  [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPad Simulator" ] )


#define GET_WIDTH  (int)[UIScreen mainScreen].bounds.size.width
#define GET_HEIGHT (int)[UIScreen mainScreen].bounds.size.height

#define kDownloadURL @"http://challenge.superfling.com/"

#define kImgDownloadURL @"http://challenge.superfling.com/photos/%@"

#define kKeyID @"ID"
#define kKeyImageID @"ImageID"
#define kKeyTitle @"Title"
#define kKeyUserID @"UserID"
#define kKeyUserName @"UserName"


#define kSFTKeyID @"sfID"
#define kSFTKeyImageID @"sfImageID"
#define kSFTKeyTitle @"sfTitle"
#define kSFTKeyUserID @"sfUserID"
#define kSFTKeyUserName @"sfUserName"

#define kKeyIsDownloadStarted @"IsDownloadedAlreadyStarted-%@"
#define kKeyImageSize @"Img-%@"
#define kKeyImageWidth @"ImgWidth-%@"

#define kCustomRowCount 7
#define kCustomRowCountForIpad 21




#endif
