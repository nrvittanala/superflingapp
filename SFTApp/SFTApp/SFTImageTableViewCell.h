//
//  SFTImageTableViewCell.h
//  SFTApp
//
//  Created by Ram on 10/30/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *myCellIdentifier = @"TableCellIdentifier";

@interface SFTImageTableViewCell : UITableViewCell

@property IBOutlet UIImageView *sftImgView;
@property IBOutlet UILabel *sftLblTitle;
@property IBOutlet UIActivityIndicatorView *sftActivityIndicator;

@end
