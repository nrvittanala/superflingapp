//
//  ViewController.m
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import "ViewController.h"
#import "SFTDefs.h"
#import "SFTDataManager.h"
#import "SFTImageTableViewCell.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self IntializeAll];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) IntializeAll
{
    self.automaticallyAdjustsScrollViewInsets = YES;
   
    self.view.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:247.0/255 blue:142.0/255.0 alpha:0.9];
    self.sftUserInfo=[NSMutableArray array];
    
    self.dataDownloader=[[SFTDownloader alloc] init];
    self.dataDownloader.delegate=self;
    self.dicImgEntries=[NSMutableDictionary dictionary];
    [self.dataDownloader downloadData:kDownloadURL];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

// -------------------------------------------------------------------------------
//	tableView:numberOfRowsInSection:
//  Customize the number of rows in the table view.
// -------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSUInteger count = self.entries.count;
    
    // if there's no data yet, return enough rows to fill the screen
    if (count == 0)
    {
        return  (IS_IPAD)? kCustomRowCountForIpad: kCustomRowCount;
    }
    return count;
}

// -------------------------------------------------------------------------------
//	tableView:cellForRowAtIndexPath:
// -------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SFTImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myCellIdentifier forIndexPath:indexPath];;
      
    cell.backgroundColor=[UIColor clearColor];
    NSUInteger nodeCount = self.entries.count;
    
    if (nodeCount ==0 && indexPath.row==0) cell.detailTextLabel.text = @"Loading…";
    else cell.detailTextLabel.text = @"";
    // Leave cells empty if there's no data yet
    if (nodeCount > 0)
    {
        // Set up the cell representing the app
        SFTData *sftData = self.entries[indexPath.row];
        
        //cell.textLabel.text =sftData.sfTitle;
        UIImage *img=[self.dicImgEntries objectForKey:sftData.sfID];
        // Only load cached images; defer new downloads until scrolling ends
        if (img == nil)
        {
            if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
            {
                [self startImgDownload:sftData  ImageID:sftData.sfImageID];
            }
           [cell.sftLblTitle removeFromSuperview];
           [cell.sftActivityIndicator startAnimating];
            cell.sftLblTitle.alpha=0.0;
            cell.sftImgView.alpha=0.0;
            
          // [cell setNeedsDisplay];
        }
        else
        {
             cell.sftLblTitle.alpha=1.0;
            [cell.sftActivityIndicator stopAnimating];
            //[cell.sftActivityIndicator removeFromSuperview];
            cell.sftLblTitle.text=sftData.sfTitle;
            CGFloat nHeight= img.size.height;
            
            [cell.sftImgView setFrame:CGRectMake(0, 0, (GET_WIDTH), nHeight)];
            
            cell.sftImgView.image=img;
            
            [cell.sftImgView removeFromSuperview];
            [cell addSubview:cell.sftImgView];
           
            [cell.sftLblTitle removeFromSuperview];
            [cell addSubview:cell.sftLblTitle];
            
            
            if (cell.sftImgView.alpha == 0.0)
            {
                [UIView animateWithDuration:1.0f animations:^{
                    
                    cell.sftImgView.alpha=1.0;
                    
                } completion:nil];
            }
           //   [cell setNeedsDisplay];
        }
    }

    
    return cell;
}

-(void) startImgDownload:(SFTData*) sftData  ImageID:(NSString*)strImgID
{
    NSString *strKey=[NSString stringWithFormat:kKeyIsDownloadStarted,strImgID];
    NSNumber *numIsAlreadyDownloading=[self.dicImgEntries objectForKey:strKey];
    if(numIsAlreadyDownloading.boolValue)
    {
      //  NSLog(@"Download Already Started  %d",indexPath.row);
        return;
    }
    else
        [self.dicImgEntries setValue:[NSNumber numberWithBool:YES] forKey:strKey];
    [self.dataDownloader downloadImage:sftData.sfID];
    
}


#pragma mark UITableViewDelegate Methods

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat nDefaultHeight = 44;
    if (self.entries.count>0)
    {
        SFTData *sftData = (self.entries)[indexPath.row];
        UIImage *img=[self.dicImgEntries objectForKey:sftData.sfID];
        CGFloat nHeight=(img.size.height/img.size.width)*GET_WIDTH;
        if ( (nHeight<=0) || isnan(nHeight) )return nDefaultHeight;
     
        return nHeight;
    }
    else return nDefaultHeight;
    
    
}
// -------------------------------------------------------------------------------
//	loadImagesForOnscreenRows
//  This method is used in case the user scrolled into a set of cells that don't
//  have their app icons yet.
// -------------------------------------------------------------------------------
- (void)loadImagesForOnscreenRows
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        if (self.entries.count > 0)
        {
            NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
            for (NSIndexPath *indexPath in visiblePaths)
            {
                SFTData *sftData = (self.entries)[indexPath.row];
                UIImage *img=[self.dicImgEntries objectForKey:sftData.sfID];
                
                if (img == nil)
                    // Avoid the app icon download if the app already has an icon
                {
                    [self startImgDownload:sftData ImageID:sftData.sfImageID];
                }
            }
        }
        
    });
    
    
   
}




#pragma mark - UIScrollViewDelegate

// -------------------------------------------------------------------------------
//	scrollViewDidEndDragging:willDecelerate:
//  Load images for all onscreen rows when scrolling is finished.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForOnscreenRows];
    }
}

// -------------------------------------------------------------------------------
//	scrollViewDidEndDecelerating:scrollView
//  When scrolling stops, proceed to load the app icons that are on screen.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}




#pragma mark SFTDownloader Delegate Methods
-(void) OnDataDownloadFailed:(NSError*)error
{
    NSLog(@"%@",[error description]);
}
-(void) OnDataDownload:(NSDictionary*) data
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        for(NSDictionary *dic in data)
        {
           SFTData *sftData= [[SFTDataManager sharedInstance] addTaskdata:dic];
               
        }
        
        if (data.count>0) [[SFTDataManager sharedInstance] saveContext];
        
         self.entries=[[SFTDataManager sharedInstance] queryResult:nil Value:nil];
        NSLog(@"DataLoaded %lu",(unsigned long)self.entries.count);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //block to be run on the main thread
            [self.tableView reloadData];
        });
        

    });
    
}
-(void) OnDownloadImage:(UIImage*) image ImageID:(NSString *)sID Bytes:(NSUInteger)Bytes OrgWidth:(CGFloat)nWidth
{
    if (image == nil) return;
    NSLog(@"Downloaded Image ID=%@",sID);
    [self.dicImgEntries setObject:image forKey:sID];
    dispatch_async(dispatch_get_main_queue(), ^{
        //block to be run on the main thread
        [self.tableView reloadData];
    });
    
    
   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
         [self processUserData:sID Bytes:Bytes OrgWidth:nWidth];
    });

   
    
}


-(void) processUserData:(NSString*) sID Bytes:(NSUInteger)Bytes OrgWidth:(CGFloat)nWidth
{
    NSArray *arrayItems=[[SFTDataManager sharedInstance] queryResult:kSFTKeyImageID  Value:sID];
    for(SFTData *sftData in arrayItems)
    {
        NSArray *arrayItems_userid=[[SFTDataManager sharedInstance] queryResult:kSFTKeyUserID  Value:sftData.sfUserID];
        
        SFTUserData *userData= nil;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uID == %@",sftData.sfUserID];
        NSArray *newArray = [self.sftUserInfo filteredArrayUsingPredicate:predicate];
        if (newArray.count>0)
        {
            userData=[newArray lastObject];
            userData.uNumOfPosts=(int)arrayItems_userid.count;
             userData.uMaxWidth =nWidth>userData.uMaxWidth? nWidth:userData.uMaxWidth;
            [userData.arrFileImageSize addObject:[NSNumber numberWithUnsignedInteger:Bytes]];
        }
        else
        {
            userData=[[SFTUserData alloc] init];
            userData.uID=sftData.sfUserID;
            userData.uNumOfPosts =(int) arrayItems_userid.count;
            userData.uMaxWidth =nWidth>userData.uMaxWidth? nWidth:userData.uMaxWidth;
            userData.uName=sftData.sfUserName;
            [userData.arrFileImageSize addObject:[NSNumber numberWithUnsignedInteger:Bytes]];
            [self.sftUserInfo addObject:userData];
            
        }
        
        
     //   NSLog(@"sftData.UserName=%@", sftData.sfUserName);
    }

}

-(IBAction) OnBtnUserData:(id)sender
{
    [self calculateUserData];
}

-(void) calculateUserData
{
    for (SFTUserData *userData in self.sftUserInfo)
    {
        NSNumber *average = [userData.arrFileImageSize valueForKeyPath:@"@avg.self"];
        NSLog(@"User Name=%@, Number of posts=%d, Average image size=%f, Greatest photo width=%f", userData.uName, userData.uNumOfPosts, average.floatValue, userData.uMaxWidth);
    }
}

@end
