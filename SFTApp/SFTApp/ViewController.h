//
//  ViewController.h
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFTDownloader.h"

@interface SFTUserData : NSObject

@property (nonatomic,strong) NSString *uID;
@property (nonatomic,strong) NSString *uName;
@property (nonatomic,strong) NSString *uAvgFileImageSize;
@property (nonatomic) int uNumOfPosts;
@property (nonatomic) CGFloat  uMaxWidth;
@property (nonatomic,strong) NSMutableArray *arrFileImageSize;

@end

@implementation SFTUserData

-(id) init
{
    self=[super init];
    self.arrFileImageSize=[NSMutableArray array];
    return self;
}
@end

@interface ViewController : UITableViewController<SFTDownloaderDelegate>


@property (nonatomic,strong) SFTDownloader *dataDownloader;
@property (nonatomic,strong) NSMutableDictionary *dicImgEntries;
@property (nonatomic,strong) NSArray *entries;
@property (nonatomic,strong) NSMutableArray *sftUserInfo;


-(IBAction) OnBtnUserData:(id)sender;


@end

