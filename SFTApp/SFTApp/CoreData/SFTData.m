//
//  SFTData.m
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import "SFTData.h"


@implementation SFTData

@dynamic sfID;
@dynamic sfImageID;
@dynamic sfTitle;
@dynamic sfUserID;
@dynamic sfUserName;

@end
