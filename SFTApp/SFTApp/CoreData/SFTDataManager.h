//
//  SFTDataManager.h
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SFTData.h"
#import <UIKit/UIKit.h>

@interface SFTDataManager : NSObject

{
    
}

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+(SFTDataManager*) sharedInstance;
-(id) init;


-(NSArray*) getSavedData;

-(SFTData*) addTaskdata:(NSDictionary*) data;


-(NSArray*) queryResult:(NSString*)strColumn Value:(NSString*)strValue;


@end
