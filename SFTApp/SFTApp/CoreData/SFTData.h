//
//  SFTData.h
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SFTData : NSManagedObject

@property (nonatomic, retain) NSString * sfID;
@property (nonatomic, retain) NSString * sfImageID;
@property (nonatomic, retain) NSString * sfTitle;
@property (nonatomic, retain) NSString * sfUserID;
@property (nonatomic, retain) NSString * sfUserName;

@end
