//
//  SFTDataManager.m
//  SFTApp
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import "SFTDataManager.h"
#import "SFTDefs.h"

static SFTDataManager *sharedinstance;

@interface SFTDataManager()
@property (readwrite, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readwrite, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readwrite, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

@implementation SFTDataManager

+(SFTDataManager*) sharedInstance
{
    if (sharedinstance == nil)
    {
        sharedinstance=[[SFTDataManager alloc] init];
    }
    return sharedinstance;
}

-(id) init
{
    if (sharedinstance == nil)
    {
        sharedinstance=[super init];
        [self IntializeAll];
    }
    return sharedinstance;
}

-(void) IntializeAll
{
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SFTApp" withExtension:@"momd"];
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    
    // Create the coordinator and store    
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"SFTApp.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![self.persistentStoreCoordinator  addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
    
    self.managedObjectContext = [[NSManagedObjectContext alloc] init];
    [self.managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    
    
    
}



-(NSArray*) getSavedData
{
    
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SuperflingImageData"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    return fetchedObjects;
    
}

-(SFTData*) addTaskdata:(NSDictionary*) data
{
     
    NSNumber *sfID=[data valueForKey:kKeyID];
    NSNumber *sfImageID=[data valueForKey:kKeyImageID];
    NSString *sfTitle=[data valueForKey:kKeyTitle];
    NSNumber *sfUserID=[data valueForKey:kKeyUserID];
    NSString *sfUserName=[data valueForKey:kKeyUserName];
    
    
    NSArray *array=[self queryResult:kSFTKeyID Value:[sfID stringValue]];
    
    if ([array count] ==0)
    {
        SFTData *sftData=[NSEntityDescription
                          insertNewObjectForEntityForName:@"SFTData"
                          inManagedObjectContext:[self managedObjectContext]];
        
        
        sftData.sfID=  [sfID stringValue];
        sftData.sfImageID=[sfImageID stringValue];
        sftData.sfTitle=sfTitle;
        sftData.sfUserID=[sfUserID stringValue];
        sftData.sfUserName=sfUserName;
        return  sftData;
    }
    else return nil;
        
}




-(NSArray*) queryResult:(NSString*)strColumn Value:(NSString*)strValue
{
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SFTData"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    if (strValue != nil)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",strColumn,strValue];
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    return fetchedObjects;
}





#pragma mark - Core Data stack


- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Ramaraju-VN.SFTTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}




#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
@end
