//
//  SFTImageTableViewCell.m
//  SFTApp
//
//  Created by Ram on 10/30/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import "SFTImageTableViewCell.h"

@implementation SFTImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    // ignore the style argument and force the creation with style UITableViewCellStyleSubtitle
    return [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  //  NSLog(@"touchesBegan");
    if (self.sftImgView.image != nil)
    {
        [UIView transitionWithView:self
                          duration:1.0
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            [self.sftImgView removeFromSuperview];
                            [self.sftLblTitle removeFromSuperview];
                            [self addSubview:self.sftImgView ];
                            [self addSubview:self.sftLblTitle ];

                            
                        }
                        completion:nil];
    }
}


@end
