//
//  SFTAppTests.m
//  SFTAppTests
//
//  Created by Ram on 10/28/14.
//  Copyright (c) 2014 Ram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "SFTDataManager.h"
#import "SFTDownloader.h"
#import "SFTDefs.h"

@interface SFTAppTests : XCTestCase<SFTDownloaderDelegate>

@property (nonatomic,strong) XCTestExpectation *expectation;
@property (nonatomic,strong) SFTDownloader *dataDownloader;
@end

@implementation SFTAppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}



#pragma mark - SFTDataManager class singleton instance test

- (SFTDataManager *)createUniqueSFTDataManagerInstance {
    
    return [[SFTDataManager alloc] init];
    
}

- (SFTDataManager *)getSFTDataManagerSharedInstance {
    
    return [SFTDataManager sharedInstance];
    
}

- (void)testSFTDataManagerSingletonSharedInstanceCreated {
    
    XCTAssertNotNil([self getSFTDataManagerSharedInstance]);
    
}

- (void)testSFTDataManagerSingletonUniqueInstanceCreated {
    
    XCTAssertNotNil([self createUniqueSFTDataManagerInstance]);
    
}

- (void)testSFTDataManagerSingletonReturnsSameSharedInstanceTwice {
    
    SFTDataManager *s1 = [self getSFTDataManagerSharedInstance];
    XCTAssertEqual(s1, [self getSFTDataManagerSharedInstance]);
    
}

- (void)testSFTDataManagerSingletonSharedInstanceSeparateFromUniqueInstance {
    
    SFTDataManager *s1 = [self getSFTDataManagerSharedInstance];
    XCTAssertEqual(s1, [self createUniqueSFTDataManagerInstance]);
}

- (void)testSFTDataManagerSingletonReturnsSeparateUniqueInstances {
    
    SFTDataManager *s1 = [self createUniqueSFTDataManagerInstance];
    XCTAssertEqual(s1, [self createUniqueSFTDataManagerInstance]);
}


#pragma mark Check Data Downloading
-(void) testCheckDataDownloading
{
    
    self.dataDownloader=[[SFTDownloader alloc] init];
    self.dataDownloader.delegate=self;
    [self.dataDownloader downloadData:kDownloadURL];

   // NSArray *arrayEntries=[[SFTDataManager sharedInstance] queryResult:nil Value:nil];
    
    
}

#pragma mark SFTDownloader Delegate Methods
-(void) OnDataDownloadFailed:(NSError*)error
{
   
}
-(void) OnDataDownload:(NSDictionary*) data
{
    [self.expectation fulfill];
}


@end
